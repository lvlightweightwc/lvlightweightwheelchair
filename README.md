**Disability Lightweight Wheelchair LV**

At Disability Friendly we have invented a lightweight eletric wheelchair, with a new lithium-ion battery technology that allows you to go further than before. It’s an ultra lightweight electric wheelchair that only weighs 50 pounds but can support passengers up to 265 pounds in weight. While still being 25% more compact than traditional wheelchairs. Get he power of electric with the portability of light folding wheelchairs.
 [https://www.disabilityfriendlylv.com/lightweight-wheelchair](https://www.disabilityfriendlylv.com/lightweight-wheelchair) 

---

